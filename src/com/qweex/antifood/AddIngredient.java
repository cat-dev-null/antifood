package com.qweex.antifood;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class AddIngredient extends Activity implements View.OnClickListener {

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingredient);
        findViewById(R.id.save).setOnClickListener(this);

        Spinner category = ((Spinner)findViewById(R.id.category));
        DbAbstraction.DbAbstractionAdapter adapter = MyActivity.db.new DbAbstractionAdapter(
                this,
                android.R.layout.simple_spinner_item,
                MyActivity.categories.getAllRows(),
                new String[]{"name"},
                new int[]{android.R.id.text1}
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        String name = ((TextView)findViewById(R.id.name)).getText().toString();
        DbAbstraction.TableRow row = MyActivity.categories.getRow(new DbAbstraction.FieldValue("name", name));
        MyActivity.ingredients.addRow(new DbAbstraction.FieldValueArrayBuilder()
                .add("name", name)
                .belongsTo(MyActivity.categories, row)
                .build()
        );
        setResult(MyActivity.UPDATED);
        finish();
    }
}
